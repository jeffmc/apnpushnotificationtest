//
//  ApplicationDelegate.m
//  PushMeBaby
//
//  Created by Jeff McLeman
//  Copyright 2009 Verglaslabs. All rights reserved.
//

#import "ApplicationDelegate.h"

//#define USE_PRODUCTION
#undef USE_PRODUCTION

@interface ApplicationDelegate ()
@property (nonatomic, strong) NSArray *messages;
@property (nonatomic, strong) NSString *payload;
@end

@implementation ApplicationDelegate

@synthesize certificate = _certificate;

- (id)init {
	self = [super init];
	if(self != nil) {
        
        
		// self.deviceToken = @"xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxx xxxxxxxxx xxxxxxxx xxxxxxxx";
        
        // these are predefined payloads. Modify as you wish
        
		NSString *callPayload = @"{\"aps\":{\"alert\":\"Incomming Call From Godzilla\",\"sound\":\"sp-ring.aif\",\"badge\":1},\"action\":\"call\",\"source_jid\":\"14258297414@xmpp.sidecar.me\",\"timestamp\":\"123456789\"}";

        NSDictionary *callMessage = [NSDictionary dictionaryWithObjectsAndKeys:callPayload ,@"payload",
                                     @"Call",@"message",nil];
        

        NSString *infoPayload = @"{\"aps\":{\"alert\":\"Sidecar is expanding!\",\"sound\":\"pulse.aif\",\"badge\":3},\"action\":\"info\",\"source_jid\":\"14258297414@xmpp.sidecar.me\",\"timestamp\":\"123456789\"}";

        NSDictionary *infoMessage = [NSDictionary dictionaryWithObjectsAndKeys:infoPayload,@"payload",
                                     @"Info",@"message",nil];

        NSString *mediaPayload = @"{\"aps\":{\"alert\":\"Godzilla shared a thing with you\",\"sound\":\"pulse.aif\",\"badge\":3},\"action\":\"media\",\"source_jid\":\"14258297414@xmpp.sidecar.me\",\"timestamp\":\"123456789\"}";

        NSDictionary *mediaMessage = [NSDictionary dictionaryWithObjectsAndKeys:mediaPayload,@"payload",@"Media",@"message",nil];
 
        NSString *joinedPayload = @"{\"aps\":{\"alert\":\"Godzilla joined Sidecar\",\"sound\":\"pulse.aif\",\"badge\":3},\"action\":\"joined\",\"source_jid\":\"14258297414@xmpp.sidecar.me\",\"timestamp\":\"123456789\"}";

        NSDictionary *joinedMessage = [NSDictionary dictionaryWithObjectsAndKeys:joinedPayload,@"payload",
                                       @"Joined",@"message",nil];
        
        NSString *groupCallPayload = @"{\"aps\":{\"alert\":\"Group Call From Godzilla Group\",\"sound\":\"sp-ring.aif\",\"badge\":1},\"action\":\"group_call\",\"g_id\":\"2452\",\"source_jid\":\"14258297414@xmpp.sidecar.me\",\"timestamp\":\"123456789\"}";
        
        NSDictionary *groupCallMessage = [NSDictionary dictionaryWithObjectsAndKeys:groupCallPayload, @"payload",
                                          @"groupCall",@"message", nil];

        self.messages = [NSArray arrayWithObjects:callMessage, infoMessage,mediaMessage,
                         joinedMessage, groupCallMessage, nil];
        
        [self.messageSelect addItemsWithObjectValues:self.messages];

        // either use the Apple prod or dev cert
        
#if defined(USE_PRODUCTION)
        self.certificate = [[NSBundle mainBundle] pathForResource:@"apns_prod" ofType:@"cer"];
#else
        self.certificate = [[NSBundle mainBundle] pathForResource:@"apns_dev" ofType:@"cer"];
#endif
        
	}
	return self;
}




- (void)applicationDidFinishLaunching:(NSNotification *)notification {
	[self connect];
}

- (void)applicationWillTerminate:(NSNotification *)notification {
	[self disconnect];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)application {
	return YES;
}


- (void)connect {
	
	if(self.certificate == nil) {
		return;
	}
	
	OSStatus result;
	
	// connect to server.
	PeerSpec peer;
    // either use the Apple prod or dev sandbox
#if defined(USE_PRODUCTION)
    const char server[] ="gateway.push.apple.com"; 
#else    
    const char server[] = "gateway.sandbox.push.apple.com";
#endif    
    result = MakeServerConnection(server, 2195, &socket, &peer);
	NSLog(@"MakeServerConnection(): %d", result);
	
	// Create SSL context.
	result = SSLNewContext(false, &context);
	NSLog(@"SSLNewContext(): %d", result);
	
	// Set callback for SSL context
	result = SSLSetIOFuncs(context, SocketRead, SocketWrite);
	NSLog(@"SSLSetIOFuncs(): %d", result);
	
	// Set SSL context
	result = SSLSetConnection(context, socket);
	NSLog(@"SSLSetConnection(): %d", result);
	
	// Set server name
#if defined(USE_PRODUCTION)
    int port = 22;
#else
    int port = 30;
#endif
	result = SSLSetPeerDomainName(context, server, port); //port 22 for prod. port 30 fro dev
	NSLog(@"SSLSetPeerDomainName(): %d", result);
	
	// Open keychain.
	result = SecKeychainCopyDefault(&keychain);
	NSLog(@"SecKeychainOpen(): %d", result);
	
	// Create certificate in the keychain from the one in the directory.
	NSData *certificateData = [NSData dataWithContentsOfFile:self.certificate];
    CFDataRef data = CFDataCreate(NULL, [certificateData bytes], [certificateData length]);
    _certificate = CFBridgingRelease(SecCertificateCreateWithData(NULL,data));
		
	// Create identity.
	result = SecIdentityCreateWithCertificate(keychain, (__bridge SecCertificateRef)(_certificate), &identity);
	NSLog(@"SecIdentityCreateWithCertificate(): %d", result);
	
	// Set client certificate.
	CFArrayRef certificates = CFArrayCreate(NULL, (const void **)&identity, 1, NULL);
	result = SSLSetCertificate(context, certificates);
	NSLog(@"SSLSetCertificate(): %d", result);
	CFRelease(certificates);
	
	// Perform SSL handshake.
	do {
		result = SSLHandshake(context);
		NSLog(@"SSLHandshake(): %d", result);
	} while(result == errSSLWouldBlock);
	
}

- (void)disconnect {
	
	if(self.certificate == nil) {
		return;
	}
	
	OSStatus result;
	
	// Close SSL session.
	result = SSLClose(context);
	NSLog(@"SSLClose(): %d", result);
	
	// cleanup
	CFRelease(identity);
	//CFRelease(certificate);
	CFRelease(keychain);
	
	// Close connection
	close((int)socket);
	
	// Delete SSL context.
	result = SSLDisposeContext(context);
	NSLog(@"SSLDisposeContext(): %d", result);
	
}


- (IBAction)push:(id)sender {
	
	if(self.certificate == nil) {
		NSLog(@"ButtonPush: No SSL cert!");
		return;
	}
	
	// Validate input.
	if(self.deviceToken == nil ) {
		NSLog(@"ButtonPush: No input data");
		return;
	}
	
	// Convert string into device token data.
	NSMutableData *deviceToken = [NSMutableData data];
	unsigned value;
	NSScanner *scanner = [NSScanner scannerWithString:self.deviceToken];
	while(![scanner isAtEnd]) {
		[scanner scanHexInt:&value];
		value = htonl(value);
		[deviceToken appendBytes:&value length:sizeof(value)];
	}
	
	// Create C input variables.
	char *deviceTokenBinary = (char *)[deviceToken bytes];
	char *payloadBinary = (char *)[self.payload UTF8String];
	size_t payloadLength = strlen(payloadBinary);
    // Define some variables.
	uint8_t command = 0;
	char message[293];
	char *pointer = message;
	uint16_t networkTokenLength = htons(32);
	uint16_t networkPayloadLength = htons(payloadLength);
	
	// Compose message.
	memcpy(pointer, &command, sizeof(uint8_t));
	pointer += sizeof(uint8_t);
	memcpy(pointer, &networkTokenLength, sizeof(uint16_t));
	pointer += sizeof(uint16_t);
	memcpy(pointer, deviceTokenBinary, 32);
	pointer += 32;
	memcpy(pointer, &networkPayloadLength, sizeof(uint16_t));
	pointer += sizeof(uint16_t);
	memcpy(pointer, payloadBinary, payloadLength);
	pointer += payloadLength;
	
	// Send message over SSL.
	size_t processed = 0;
	OSStatus result = SSLWrite(context, &message, (pointer - message), &processed);
	NSLog(@"SSLWrite(): %d %zu", result, processed);
	
}

- (NSInteger)numberOfItemsInComboBox:(NSComboBox *)aComboBox {
    
    return [self.messages count];
}
- (id)comboBox:(NSComboBox *)aComboBox objectValueForItemAtIndex:(NSInteger)index {
    
    NSDictionary *dict = [self.messages objectAtIndex:index];
    return [dict objectForKey:@"message"];
}



- (void)comboBoxWillPopUp:(NSNotification *)notification {
    NSLog(@"Combo box up");
}
- (void)comboBoxWillDismiss:(NSNotification *)notification {
    NSLog(@"Combo box gone");
 
}
- (void)comboBoxSelectionDidChange:(NSNotification *)notification {
    
}
- (void)comboBoxSelectionIsChanging:(NSNotification *)notification {
    NSLog(@"[%@ %@] value == %@", NSStringFromClass([self class]),
          NSStringFromSelector(_cmd), [self.messages objectAtIndex:
                                       [(NSComboBox *)[notification object] indexOfSelectedItem]]);
    NSDictionary *dict = [self.messages objectAtIndex:
                           [(NSComboBox *)[notification object] indexOfSelectedItem]];
    
    self.payload =  [dict objectForKey:@"payload"];
}

@end
