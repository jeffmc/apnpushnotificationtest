//
//  ApplicationDelegate.h
//  PushMeBaby
//
//  Created by Jeff McLeman
//  Copyright 2009 VerglasLabs. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "ioSock.h"

@interface ApplicationDelegate : NSObject <NSComboBoxDelegate,NSComboBoxDataSource> {
	otSocket socket;
	SSLContextRef context;
	SecKeychainRef keychain;
	SecCertificateRef certificate;
	SecIdentityRef identity;
    BOOL _useProduction;

}
@property(nonatomic, strong) NSString *deviceToken, *certificate ;
@property (nonatomic, strong) IBOutlet NSComboBox *messageSelect;
@property (nonatomic, strong) NSData *dataPayload;

- (void)connect;

- (void)disconnect;

- (IBAction)push:(id)sender;

@end
