//
//  main.m
//  PushNotificationTest
//
//  Created by Jeff McLeman on 4/19/09.
//  Copyright Verglas Software 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc,  (const char **) argv);
}
